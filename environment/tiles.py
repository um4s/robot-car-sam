class Fin:
    """
    This class aims to avoid AttributeErrors when player tries to access Tile's properties on an off-border position.
    As a result, all cars successfully getting out the map ends in this tile.
    """
    def __init__(self):
        self.type = "Fin"
        self.occupants = []
        self.coordonnees = {"x": -1, "y": -1}


class Tuile:
    def __init__(self, props, x_pos, y_pos, end_tile):
        """
        Class constructor, returns an instance.
        :param props: properties extracted from level file.
        :param x_pos: X coordinate in the level map grid.
        :param y_pos: Y coordinate in the level map grid.
        :param end_tile: The End tile instance on which cars arrived when the reach map's border.
        """
        self.occupants = []
        self.tuiles_voisines = {"N": end_tile, "S": end_tile, "E": end_tile, "O": end_tile}
        self.types_voisins = {"N": "Fin", "S": "Fin", "E": "Fin", "O": "Fin"}

        self.coordonnees = {"x": x_pos, "y": y_pos}
        self.type = props["type"]
        if self.type == "Route":
            self.directions = props["directions"]

    def get_image(self) -> str:
        """
        Determines the tile's image depending on its neighbors.
        :return: image name to be used to represent this tile
        """
        if self.type == "Herbe":
            return "images/herbe.png"
        if self.type == "Route":
            conn_types = ["Route", "Fin"]
            connexions = "".join(["R" if self.types_voisins[orn] in conn_types else "0" for orn in "NESO"])
            if connexions == "R00R":
                return "images/coin-ON.png"
            elif connexions == "RR00":
                return "images/coin-NE.png"
            elif connexions == "0RR0":
                return "images/coin-ES.png"
            elif connexions == "00RR":
                return "images/coin-SO.png"

            return "images/route.png"
        return "images/void.png"

    def update_neighbour(self, direction: str, tile) -> None:
        """
        Updates the neighbours tiles properties and the types signature.
        :param direction: Indicate the relative direction of the neighbour tile.
        :param tile: The next tile object in the given direction (N, S, E, W).
        :return: None
        """
        self.tuiles_voisines[direction] = tile
        self.types_voisins[direction] = tile.type
