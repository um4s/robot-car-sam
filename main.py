from sys import exit
import csv
from typing import Any, Literal, List
import pygame
from pygame.locals import *

from environment.map import LevelMap
from environment.simulation import Simulator

# Paramètres de largeur et hauteur
WIDTH = 800
HEIGHT = 600
TILE_WIDTH = 100

# Custom Events type codes
ACTOR_TICK = USEREVENT

# Initialisation de Pygame
pygame.init()  # Initialisation du service
screen = pygame.display.set_mode((WIDTH, HEIGHT))  # Initialisation de la fenêtre de jeu
pygame.display.set_caption("Robot Cars")  # Définition du titre de la fenêtre
clock = pygame.time.Clock()  # Initialisation de l'horloge

# Initialisation des données du jeu
levels = {
    "level_1": {"title": "Niveau 1", "filename": "level1.dat"},
    "level_2": {"title": "Niveau 2", "filename": "level2.dat"},
    "level_3": {"title": "Niveau 3", "filename": "level3.dat"},
    "level_4": {"title": "Niveau 4", "filename": "level4.dat"},
    "level_5": {"title": "Niveau 5", "filename": "level5.dat"},
}
level_ids = ""
level_index = 0
selected_level_id = ""
toDebrief = False


def read_level_data(level_filename: str) -> List[List[str]]:
    """
    Reads the level file as per provided filename
    :param level_filename: Filename of the level file to load
    :return: Table of dicts representing each level tile
    """
    level_data: List[List[str]] = []
    level_file_path = "levels/" + level_filename
    with open(level_file_path, "r") as level_file:
        level_reader = csv.reader(level_file, delimiter=";")
        for row in level_reader:
            level_data.append([])  # Adding a new list for row
            for cell in row:
                level_data[-1].append(eval(cell))

    return level_data


def init_map(level: dict) -> LevelMap:
    """
    Creates tiles objects as per level data details
    :param level: dict info of selected level
    :return: list : Grid of tiles objects created through map object
    """
    level_data = read_level_data(level["filename"])
    # Instanciate map object
    map_object = LevelMap(level_data)
    return map_object


def select_level(games_levels):
    global level_ids, level_index

    menu_screen = pygame.display.set_mode((800, 600))
    font = pygame.font.SysFont(None, 72)

    background_surf = pygame.image.load("images/menu_back.png").convert()

    level_ids = list(games_levels.keys())
    level_index = 0
    while True:
        for menu_event in pygame.event.get():
            if menu_event.type == QUIT:
                # Dés-initialisation du service pygame
                pygame.quit()
                # Fermeture du programme python
                exit()
            if menu_event.type == KEYUP:
                if menu_event.key == K_ESCAPE:
                    pygame.quit()
                    exit()
                if menu_event.key == K_RIGHT or menu_event.key == K_UP:
                    level_index += 1
                    if level_index == len(level_ids):
                        level_index = 0
                if menu_event.key == K_LEFT or menu_event.key == K_DOWN:
                    level_index -= 1
                    if level_index == -1:
                        level_index = len(level_ids) - 1
                if menu_event.key == K_RETURN:
                    return level_ids[level_index]
        # Displays background
        menu_screen.blit(background_surf, (0, 0))
        # Displays level title
        textobj = font.render(levels[level_ids[level_index]]["title"], 1, "yellow")
        textrect = textobj.get_rect(center=(400, 300))
        menu_screen.blit(textobj, textrect)

        pygame.display.update()


def debrief_screen() -> str:
    # declare variables
    global WIDTH, HEIGHT, level_ids, level_index, toDebrief
    buttonColor = (255, 220, 10)
    backColor = (52, 59, 69)
    fontColor = (250, 250, 250)
    button_width, button_height = 200, 60
    button_x = (WIDTH - button_width) // 2
    button_y = HEIGHT - 150
    running = True

    def draw_button(text, x, y, w, h, color, screen) -> None:
        radius = h // 2
        # rectangle central sans les coins arrondis
        center_rect = pygame.Rect(x + radius, y, w - 2 * radius, h)
        pygame.draw.rect(screen, color, center_rect)

        # Dessiner les demi-cercles aux deux extrémités
        pygame.draw.circle(screen, color, (x + radius, y + radius), radius)
        pygame.draw.circle(screen, color, (x + w - radius, y + radius), radius)

        font = pygame.font.SysFont("None", 35)
        label = font.render(text, True, (0, 0, 0))
        screen.blit(
            label, (x + (w - label.get_width()) / 2, y + (h - label.get_height()) / 2)
        )

    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    lvl = levels[selected_level_id]["title"]
    if lvl != 5:
        txt = f"{lvl} terminé"
    elif lvl == 5:
        txt = f"{lvl} terminé, Bravo c'était le dernier niveau"
    while running:
        screen.fill(backColor)
        font = pygame.font.SysFont("None", 50)

        message = font.render("Félicitations", True, fontColor)
        screen.blit(message, (WIDTH // 2 - message.get_width() // 2, HEIGHT // 4))

        level_text = font.render(txt, True, fontColor)
        screen.blit(
            level_text, (WIDTH // 2 - level_text.get_width() // 2, HEIGHT // 4 + 60)
        )
        draw_button(
            "Niveau Suivant",
            button_x,
            button_y,
            button_width,
            button_height,
            buttonColor,
            screen,
        )
        for event in pygame.event.get():  # surveilles les actions faites par l'user
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                x, y = pygame.mouse.get_pos()
                if (
                    button_x < x < button_x + button_width
                    and button_y < y < button_y + button_height
                ):
                    level_index += 1
                    if level_index == len(level_ids):
                        level_index = 0
                    return level_ids[level_index]

        pygame.display.flip()
    return "autre"


def display_game_board(map_object: LevelMap):
    for y in range(map_object.get_height()):
        for x in range(map_object.get_width()):
            tile = map_object.get_tile(x, y)
            # Creating images and rectangles for each tiles
            image_path = tile.get_image()
            tile.surface = pygame.image.load(image_path).convert()
            tile.rectangle = tile.surface.get_rect(topleft=(x * 100, y * 100))
            # Adding tiles to objects
            map_object.modified_tiles[tile] = True


# INITIATING GAME VARIABLES
# -------------------------
display_menu = True
# Time variables
next_tick = 0
game_start = 0
update_delay = 800  # in milliseconds
# Fails and Success
success = False
accidents = 0
off_roads = 0
# Pygame objects
actors_images = {"car": "images/pink_car.png"}
actors_surfaces = {
    actor: pygame.image.load(img_filename).convert_alpha()
    for actor, img_filename in actors_images.items()
}

while True:
    # Parcours de la liste des évènements
    for event in pygame.event.get():
        if event.type == KEYUP:
            if event.key == K_ESCAPE:
                display_menu = True
        # Si l'utilisateur ferme le programme
        elif event.type == QUIT:
            # Dés-initialisation du service pygame
            pygame.quit()
            # Fermeture du programme python
            exit()
        elif event.type >= ACTOR_TICK:
            simulator.move_actor(event.actor)

    # Displaying menu if necessary
    if display_menu or event.type == KEYUP and event.key == K_ESCAPE:
        if toDebrief:
            pass
        else:
            selected_level_id = select_level(levels)
        # Loading level data
        level_map = init_map(levels[selected_level_id])
        simulator: Simulator = Simulator(level_map, actors_surfaces, ACTOR_TICK)
        # Defining windows dimensions
        w, h = level_map.get_width() * TILE_WIDTH, level_map.get_height() * TILE_WIDTH
        screen = pygame.display.set_mode((w, h))
        # Creating pygames tiles objects
        display_game_board(level_map)

        display_menu = False
        game_start = pygame.time.get_ticks()
        next_tick = game_start + update_delay

    # Iterate through actors and apply their actions
    if pygame.time.get_ticks() > next_tick:
        next_tick = pygame.time.get_ticks() + update_delay

        if len(simulator.cars_location) == 0:
            selected_level_id = debrief_screen()
            toDebrief = True
            display_menu = True

        counter = 0

    # Drawing modified tiles and Objects
    for tile in level_map.modified_tiles.keys():
        screen.blit(tile.surface, tile.rectangle)

        # Displaying road directions
        if tile.type == "Route":
            for dir in tile.directions:
                dir_angle = "NOSE".index(dir) * 90
                dir_image = pygame.image.load(
                    "images/road_direction.png"
                ).convert_alpha()
                dir_surface = pygame.transform.rotate(dir_image, dir_angle)
                dir_rect = dir_surface.get_rect(
                    center=(TILE_WIDTH // 2, TILE_WIDTH // 2)
                )
                # Moving icon to its tile
                dir_rect.x += tile.coordonnees["x"] * TILE_WIDTH
                dir_rect.y += tile.coordonnees["y"] * TILE_WIDTH
                screen.blit(dir_surface, dir_rect)

        # Displaying occupants
        if len(tile.occupants) >= 1:
            car = tile.occupants[0]
            car_image = car.surface
            if len(tile.occupants) > 1:
                car_image = pygame.image.load("images/accident.png").convert_alpha()
                car = tile.occupants[-1]
            elif tile.type in ("Herbe",):
                car_image = pygame.image.load("images/car_out.png").convert_alpha()
            # Rotating occupants to its direction
            direction_angle = "NOSE".index(car.direction) * 90
            rotated_surface = pygame.transform.rotate(car_image, direction_angle)
            rotated_rect = rotated_surface.get_rect(
                center=(TILE_WIDTH // 2, TILE_WIDTH // 2)
            )
            # Moving occupants to its tile
            rotated_rect.x += tile.coordonnees["x"] * TILE_WIDTH
            rotated_rect.y += tile.coordonnees["y"] * TILE_WIDTH

            # Displaying occupants
            screen.blit(rotated_surface, rotated_rect)

    level_map.modified_tiles = {}

    # Updating display
    pygame.display.update()
    clock.tick(60)  # Limite la vitesse d'affichage (framerate)
