from tkinter import N
from turtle import pos
from environment.tiles import Tuile
import time


class Voiture:
    def __init__(self) -> None:
        self.direction = "N"  # La direction de la voiture est corrigée lors de son instanciation sur sa tuile de départ

    def tourner(self, position: Tuile) -> bool:
        """
        Le jeu interroge la voiture pour déterminer si elle souhaite tourner à gauche.
        :param position : Objet Tuile sur laquelle se trouve la voiture.
        :return: Vrai ou Faux selon qu'il faille tourner à gauche ou pas.
        """
        toGo = position.directions  # récupère les flèches sur les cases au sol
        if len(toGo) == 1:
            self.direction = toGo
        else:  # vérifie les cases aux alentours dans la direction toGO pour suivre celle qui est une route
            if position.tuiles_voisines[toGo[0]].type == "Route":
                self.direction = toGo[0]
            elif position.tuiles_voisines[toGo[1]].type == "Route":
                self.direction = toGo[1]
        return False

    def avancer(self, position: Tuile) -> bool:
        """
        Le jeu interroge la voiture pour déterminer si elle souhaite avancer.
        :param position : Objet Tuile sur laquelle se trouve la voiture.
        :return: Vrai ou Faux selon qu'il faille avancer ou pas.
        """
        if position.tuiles_voisines[self.direction].occupants != []:
            return False
        return True
